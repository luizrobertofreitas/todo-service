
package info.lizo.model.repository;

import info.lizo.model.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author luiz
 */
@Repository
public interface TodoRepository extends JpaRepository<Project, Long> {
    
}
