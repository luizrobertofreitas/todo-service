
package info.lizo.resources.todo.v1;

import info.lizo.model.entity.Project;
import info.lizo.model.entity.Task;
import info.lizo.model.repository.TodoRepository;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author luiz
 */
@RestController
@CrossOrigin
@RequestMapping("/projects")
public class ProjectResource {
    
    @Autowired
    private TodoRepository todoRepo;
    
    @RequestMapping(method = RequestMethod.GET)
    public List<ProjectVO> all() {
        return mapProjectsToProjectsVO(todoRepo.findAll());  
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ProjectVO save(@RequestBody final ProjectVO project) {
        
        Project projectToSave;
        
        if (project != null && todoRepo.exists(project.getId())) {
            projectToSave = todoRepo.findOne(project.getId());
            
            projectToSave.setTasks(new ArrayList<>());
            
            if (project.getTasks() != null) {
                for (TaskVO tVo : project.getTasks()) {
                    Task t = mapTaskVOToTask(tVo);
                    t.setProject(projectToSave);
                    projectToSave.getTasks().add(t);
                }
            }
        }
        else {
            projectToSave = mapProjectVOToProject(project);
        }
        
        return mapProjectToProjectVO(todoRepo.save(projectToSave));
    }
    
    public List<ProjectVO> mapProjectsToProjectsVO(final List<Project> projects) {
        List<ProjectVO> vos = new ArrayList<>();
        
        if (projects != null) {
            for (Project p : projects) {
                vos.add(mapProjectToProjectVO(p));
            }
        }
        
        return vos;
    }
    
    public Project mapProjectVOToProject(final ProjectVO vo) {
        if (vo != null && !StringUtils.isEmpty(vo.getTitle())) {
            Project p = new Project();
            
            p.setTitle(vo.getTitle());
            
            if (!CollectionUtils.isEmpty(vo.getTasks())) {
                
                p.setTasks(new ArrayList<>());
                
                for (TaskVO tVo : vo.getTasks()) {
                    p.getTasks().add(mapTaskVOToTask(tVo));
                }
            }
            
            return p;
        }
        
        return null;
    }
    
    public Task mapTaskVOToTask(final TaskVO vo) {
        if (vo != null && !StringUtils.isEmpty(vo.getTitle())) {
            Task t = new Task();
            
            t.setTitle(vo.getTitle());
            t.setId(vo.getId());
            
            return t;
        }
        
        return null;
    }
    
    public ProjectVO mapProjectToProjectVO(final Project project) {
        ProjectVO vo = new ProjectVO();

        if (project != null) {
            vo.setId(project.getId());
            vo.setTitle(project.getTitle());
            
            vo.setTasks(new ArrayList<>());
            
            if (project.getTasks() != null) {
                for (Task t : project.getTasks()) {
                    vo.getTasks().add(mapTaskToTaskVO(t));
                }
            }
        }
        
        return vo;
    }
    
    public TaskVO mapTaskToTaskVO(final Task task) {
        TaskVO vo = new TaskVO();
        
        if (task != null) {
            vo.setId(task.getId());
            vo.setTitle(task.getTitle());
        }
        
        return vo;
    }
}
